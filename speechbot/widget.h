﻿#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include <QAudioInput>
#include <QAudioOutput>
#include <QBuffer>
#include <QNetworkAccessManager>
#include "baiduspeech.h"

QT_BEGIN_NAMESPACE
namespace Ui { class Widget; }
QT_END_NAMESPACE

class Widget : public QWidget
{
    Q_OBJECT

public:
    Widget(QWidget *parent = nullptr);
    ~Widget();

private slots:
    void on_talkButton_toggled(bool checked);
    void asr_finished_slot();
    void tts_finished_slot();
    void play_state_slot(QAudio::State s);
    void on_reply(QNetworkReply*);

private:
    Ui::Widget *ui;
    QAudioInput* recorder;
    QAudioOutput* player;
    QBuffer* asrbuf;
    QBuffer* ttsbuf;
    BaiduSpeech* bds;
    QNetworkAccessManager* client;
};
#endif // WIDGET_H
