﻿#include "widget.h"
#include "ui_widget.h"
#include <QDebug>
#include <QFileDialog>
#include <QBuffer>

Widget::Widget(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::Widget)
{
    ui->setupUi(this);

    QAudioFormat format;
    format.setSampleRate(16000);//设置声音的采样频率
    format.setChannelCount(1);//设置单声道
    format.setSampleSize(16);//16位深
    format.setSampleType(QAudioFormat::SignedInt);
    format.setByteOrder(QAudioFormat::LittleEndian);
    format.setCodec("audio/pcm");

    recorder = new QAudioInput(format, this);
    player = new QAudioOutput(format, this);
    connect(player, &QAudioOutput::stateChanged, this, &Widget::play_state_slot);

    bds = new BaiduSpeech(this, "rKCHBLmYiFPuCQTS0HttLbUD", "037dc446820ec143d1628c20146b9d34");
    connect(bds, &BaiduSpeech::doneASR, this, &Widget::asr_finished_slot);
    connect(bds, &BaiduSpeech::doneTTS, this, &Widget::tts_finished_slot);
}

Widget::~Widget()
{
    delete ui;
}

void Widget::on_listenButton_toggled(bool checked)
{
    if (checked) {//开始录音
        ui->listenButton->setText("Stop");
        asrbuf = new QBuffer;
        asrbuf->open(QIODevice::ReadWrite);
        recorder->start(asrbuf);
    }
    else {//停止录音
        ui->listenButton->setText("Listen");
        recorder->stop();
        bds->doASR(asrbuf->data());
    }
}

void Widget::asr_finished_slot()
{
    ui->textBrowser->setText(bds->message());
    asrbuf->deleteLater();
}

void Widget::on_speakButton_clicked()
{
    bds->doTTS(ui->textEdit->toPlainText());
}

void Widget::tts_finished_slot()
{
    ttsbuf = new QBuffer;
    ttsbuf->open(QIODevice::ReadWrite);
    ttsbuf->write(bds->audio());
    ttsbuf->reset();

    player->start(ttsbuf);
}

void Widget::play_state_slot(QAudio::State s)
{
    if (s == QAudio::IdleState) {
        player->stop();
        ttsbuf->close();
        ttsbuf->deleteLater();
    }
}
