#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include <QNetworkAccessManager>

QT_BEGIN_NAMESPACE
namespace Ui { class Widget; }
QT_END_NAMESPACE

class Widget : public QWidget
{
    Q_OBJECT

public:
    Widget(QWidget *parent = nullptr);
    ~Widget();

private slots:
    void on_transButton_clicked();
    void on_reply(QNetworkReply*);

private:
    Ui::Widget *ui;
    QNetworkAccessManager* client;
};
#endif // WIDGET_H
